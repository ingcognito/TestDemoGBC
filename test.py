import subprocess
from datetime import datetime, timedelta
from test_config import prompts
import random

# WOW THIS ACTUALLY WORKS
# MUCH NICE
# THANK YOU INTERNET STRANGER
# https://fabianlee.org/2019/09/15/python-getting-live-output-from-subprocess-using-poll/
process = subprocess.Popen(
    './compiledbinary', stdin=subprocess.PIPE, stdout=subprocess.PIPE)

# Poll process.stdout to show stdout live
end_time = datetime.now() + timedelta(seconds=5)
while datetime.now() < end_time:
    output = process.stdout.readline()
    if process.poll() is not None:
        break
    if output:
        line = output.strip().decode()
        print(line)
        for question in prompts:
            if line == question["prompt"]:
                if question['input'] == "random_number":
                    answer = f"{random.randint(1,100)}\n".encode()
                else:
                    answer = f"{question['input']}\n".encode()

                process.stdin.write(answer)
                print(answer.decode())
                process.stdin.flush()
        # TODO: Capture that it has answered all the prompts and return a mark accordingly
rc = process.poll()
